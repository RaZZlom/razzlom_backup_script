#!/usr/bin/python

import os
import time
import hashlib
import logging

import boto3
import gnupg

### SETTINGS ##################################################################

db_name = ''
bucket_name = ''
s3_provider = "" #aws or selectel
recipient = ''
hash_type = 'sha512'  # md5 or sha512
logs_path = '/tmp/'
dumpfile_name = time.strftime('%Y_%m_%d_%H_%M', time.localtime())
encrypted_file_name = f'{dumpfile_name}.asc'
md5_file_name = f'{dumpfile_name}.asc.md5'
sha512_file_name = f'{dumpfile_name}.asc.sha512'


###############################################################################

### FUNCTIONS #################################################################


def logger_up():
    log_file_name = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    logging.basicConfig(filename=f'{logs_path}{log_file_name}.log', level=logging.INFO)
    logging.info('Start logger!')


def create_db_dump():
    # More about dump command
    # https://www.postgresql.org/docs/current/backup-dump.html

    command = f'pg_dump {db_name} > {dumpfile_name}'
    logging.info(f'Creating {db_name} dump. File - {dumpfile_name}')
    os.system(command)


def encrypt_file(file_name, recipient):
    gpg = gnupg.GPG()

    with open(f'{file_name}', 'rb') as f:
        logging.info(f'Encrypt {file_name}.')
        gpg.encrypt_file(
            file=f,
            recipients=[f'{recipient}'],
            output=f'{encrypted_file_name}', )


def get_hash_md5(file_name):
    logging.info(f'Getting {file_name} md5 hash.')
    with open(file_name, 'rb') as f:
        m = hashlib.md5()
        while True:
            data = f.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


def get_hash_sha512(file_name):
    logging.info(f'Getting {file_name} sha512 hash.')
    with open(file_name, 'rb') as f:
        m = hashlib.sha512()
        while True:
            data = f.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


def create_hash_file():
    if hash_type == 'md5':
        logging.info(f'Creating md5 file {md5_file_name}')
        md5_file = open(md5_file_name, 'w')
        md5_file.write(get_hash_md5(f'{encrypted_file_name}'))
        md5_file.close()
    else:
        logging.info(f'Creating sha512 file {sha512_file_name}')
        sha512_file = open(sha512_file_name, 'w')
        sha512_file.write(get_hash_sha512(f'{encrypted_file_name}'))
        sha512_file.close()


def s3_upload_file():
    if s3_provider == "aws":
        s3 = boto3.resource('s3')
        logging.info(f'Uploading {encrypted_file_name} to s3')
        s3.Bucket(bucket_name).upload_file(Filename=encrypted_file_name, Key=encrypted_file_name)
        if hash_type == 'md5':
            logging.info(f'Uploading {md5_file_name} to s3')
            s3.Bucket(bucket_name).upload_file(Filename=md5_file_name, Key=md5_file_name)
        else:
            logging.info(f'Uploading {sha512_file_name} to s3')
            s3.Bucket(bucket_name).upload_file(Filename=sha512_file_name, Key=sha512_file_name)
    elif s3_provider == "selectel":
        session = boto3.session.Session()
        s3 = session.client(service_name='s3', endpoint_url='https://s3.storage.selcloud.ru')
        s3.upload_file(Bucket=bucket_name, Filename=encrypted_file_name, Key=encrypted_file_name)
        if hash_type == 'md5':
            logging.info(f'Uploading {md5_file_name} to s3')
            s3.upload_file(Bucket=bucket_name, Filename=md5_file_name, Key=md5_file_name)
        else:
            logging.info(f'Uploading {sha512_file_name} to s3')
            s3.upload_file(Bucket=bucket_name, Filename=sha512_file_name, Key=sha512_file_name)


# def s3_download_file(file_name):
#     s3 = boto3.resource('s3')
#     s3.Object(bucket_name, file_name).download_file(file_name)


def delete_local_files():
    logging.info(f'Deleting {dumpfile_name} from disk')
    os.remove(dumpfile_name)
    logging.info(f'Deleting {encrypted_file_name} from disk')
    os.remove(encrypted_file_name)
    if hash_type == 'md5':
        logging.info(f'Deleting {md5_file_name} from disk')
        os.remove(md5_file_name)
    else:
        logging.info(f'Deleting {sha512_file_name} from disk')
        os.remove(sha512_file_name)


###############################################################################


if __name__ == '__main__':
    logger_up()
    create_db_dump()
    encrypt_file(dumpfile_name, recipient)
    create_hash_file()
    s3_upload_file()
    delete_local_files()
