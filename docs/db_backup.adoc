= db_backup
:toc:

This script i wrote to backup my https://github.com/syuilo/misskey[Misskey] instance database to Amazon S3.

== Getting Started

Install boto3 library:

[source, sh]
----
pip install -r requirements.txt
----

After installing boto3 set up credentials (in e.g. ~/.aws/credentials):

[source, sh]
----
[default]
aws_access_key_id = YOUR_KEY
aws_secret_access_key = YOUR_SECRET
----

Then, set up a default region (in e.g. ~/.aws/config):

[source, sh]
----
[default]
region=us-east-1
----

Clone git repository:

[source, sh]
----
git clone https://codeberg.org/RaZZlom/razzlom_backup_script.git
----

Change default settings:

[source, python]
----
db_name = 'YOUR_DATABASE_NAME'
bucket_name = 'YOUR_BUCKET_NAME'
recipient = 'YOUR_GPG_KEY'
hash_type = 'sha512'  # md5 or sha512
logs_path = './logs/'
----

Then execute script with cron or systemd.timer every hour.

== What script do:

* Makes postgres database dump
* Encrypts dump with GPG key
* Creates md5 or sha512 checksum file
* Uploads encrypted file and checksum file to s3
* Deletes dump, encrypted file and checksum file from disk

To delete old backups from s3 you need to configure bucket *Lifecycle
rules*.

== Contact Me

Fediverse - https://quietplace.xyz/@razzlom +
Email - razzlom@riseup.net
