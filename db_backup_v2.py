""" Script for backuping postgresql database to s3 """

# IMPORTS #####################################################################

import argparse
import configparser
import logging
from os import chdir, makedirs, remove, system
from os.path import join
from time import localtime, strftime

# VARIABLES ###################################################################

DB_NAME: str = ''
RECIPIENT: str = ''
RCLONE_DESTINATION: str = ''
DUMPFILE_NAME: str = strftime('%Y_%m_%d_%H_%M_%S', localtime())
WORKING_DIR: str = ''
LOGS_DIR: str = '/tmp/db_backup_logs/'

# FUNCTIONS ###################################################################


def create_db_dump() -> None:
    """ Create dump of postgres database. """

    # More about dump command
    # https://www.postgresql.org/docs/current/backup-dump.html

    chdir(WORKING_DIR)
    command: str = f'pg_dump {DB_NAME} > {DUMPFILE_NAME}'
    logging.info('Creating database dump file.')
    system(command)


def create_hash() -> None:
    """ Create hash file of database dump file. """

    chdir(WORKING_DIR)
    command: str = f'sha512sum {DUMPFILE_NAME} > {DUMPFILE_NAME}.sha512'
    logging.info('Creating hash file.')
    system(command)


def encrypt_file() -> None:
    """ Encrypt database dump file with GPG. """

    chdir(WORKING_DIR)
    command: str = f'gpg --compress-level 0 --bzip2-compress-level 0 --yes --no-armor --recipient {RECIPIENT} --encrypt {DUMPFILE_NAME}'
    logging.info('Encrypting database dump file.')
    system(command)


def upload_to_s3(filename: str) -> None:
    """ Upload file to s3 bucket. """

    chdir(WORKING_DIR)
    command: str = f'rclone copy {filename} {RCLONE_DESTINATION}'
    logging.info('Uploading %s to s3.', filename)
    system(command)


def delete_file(filename: str) -> None:
    """ Delete file """

    chdir(WORKING_DIR)
    logging.info('Deleting %s .', filename)
    remove(filename)

# MAIN ########################################################################


if __name__ == '__main__':

    # Create logs dir
    try:
        makedirs(LOGS_DIR)
    except FileExistsError:
        pass

    # Logger
    log_format: str = '%(asctime)s %(message)s'
    log_file_name: str = f'{strftime("%Y_%m_%d_%H_%M_%S", localtime())}.log'
    log_file_path: str = join(LOGS_DIR, log_file_name)
    logging.basicConfig(format=log_format,
                        filename=log_file_path, level=logging.INFO)

    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, help='Path to config file.')
    logging.info('Parsing command line arguments.')
    args = parser.parse_args()
    logging.info('%s', args)

    # Config parser
    if args.config:
        config = configparser.ConfigParser()
        logging.info('Parsing config file.')
        config.read(args.config)
        DB_NAME = config['db_backup']['db_name']
        RECIPIENT = config['db_backup']['recipient']
        RCLONE_DESTINATION = config['db_backup']['rclone_destination']
        WORKING_DIR = config['db_backup']['working_dir']
    else:
        print('Script running without configuration file. Please use "-- config" argument.')

    # Create working dir
    try:
        makedirs(WORKING_DIR)
        logging.info('Created %s.', WORKING_DIR)
    except FileExistsError:
        logging.info('%s exist. Skipping.', WORKING_DIR)
    except FileNotFoundError as e:
        logging.info('%s', e)

    create_db_dump()
    create_hash()
    encrypt_file()
    delete_file(DUMPFILE_NAME)
    upload_to_s3(f'{DUMPFILE_NAME}.gpg')
    upload_to_s3(f'{DUMPFILE_NAME}.sha512')
    delete_file(f'{DUMPFILE_NAME}.gpg')
    delete_file(f'{DUMPFILE_NAME}.sha512')
