""" Script for backuping configuration files to s3 """

# IMPORTS #####################################################################

import argparse
import configparser
import logging
from os import chdir, makedirs, remove
from os.path import join
from subprocess import run
from time import localtime, strftime

# VARIABLES ###################################################################

RECIPIENT: str = ''
RCLONE_DESTINATION: str = ''
ARCHIVE_NAME: str = strftime('%Y_%m_%d_%H_%M_%S', localtime())
WORKING_DIR: str = ''
LOGS_DIR: str = '/tmp/config_backup_logs/'

# FUNCTIONS ###################################################################


def create_archive() -> None:
    """ Create archive of configuration files. """

    files_list: str = ''

    for file_str in backup_list:
        files_list = files_list + ' ' + file_str

    files_list = files_list.replace('\n', '')

    chdir(WORKING_DIR)
    logging.info('Creating archive file.')
    run(['bash', '-c',
        f'tar --no-auto-compress --create --file={ARCHIVE_NAME}.tar {files_list}'], check=True)


def create_hash() -> None:
    """ Create hash file of archive file. """

    chdir(WORKING_DIR)
    logging.info('Creating hash file.')
    run(['bash', '-c',
        f'sha512sum {ARCHIVE_NAME}.tar > {ARCHIVE_NAME}.tar.sha512'], check=True)


def encrypt_file() -> None:
    """ Encrypt archive file with GPG. """

    chdir(WORKING_DIR)
    logging.info('Encrypting archive file.')
    run(['bash', '-c',
        f'gpg --compress-level 0 --bzip2-compress-level 0 --yes --no-armor --recipient {RECIPIENT} --encrypt {ARCHIVE_NAME}.tar'], check=True)


def upload_to_s3(filename: str) -> None:
    """ Upload file to s3 bucket. """

    chdir(WORKING_DIR)
    logging.info('Uploading %s to s3.', filename)
    run(['bash', '-c',
        f'rclone copy {filename} {RCLONE_DESTINATION}'], check=True)


def delete_file(filename: str) -> None:
    """ Delete file """

    chdir(WORKING_DIR)
    logging.info('Deleting %s .', filename)
    remove(filename)

# MAIN ########################################################################


if __name__ == '__main__':

    # Create logs dir
    try:
        makedirs(LOGS_DIR)
    except FileExistsError:
        pass

    # Logger
    log_format: str = '%(asctime)s %(message)s'
    log_file_name: str = f'{strftime("%Y_%m_%d_%H_%M_%S", localtime())}.log'
    log_file_path: str = join(LOGS_DIR, log_file_name)
    logging.basicConfig(format=log_format,
                        filename=log_file_path, level=logging.INFO)

    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str,
                        help='Path to configuration file.')
    parser.add_argument('--backup_list', type=str,
                        help='Path to file with list of files to backup.')
    logging.info('Parsing command line arguments.')
    args = parser.parse_args()
    logging.info('%s', args)

    # Config parser
    if args.config:
        config = configparser.ConfigParser()
        logging.info('Parsing config file.')
        config.read(args.config)
        RECIPIENT = config['config_backup']['recipient']
        RCLONE_DESTINATION = config['config_backup']['rclone_destination']
        WORKING_DIR = config['config_backup']['working_dir']
    else:
        print('Script running without configuration file. Please use "--config" argument.')

    if args.backup_list:
        backup_list_file = open(args.backup_list, 'r')
        backup_list: list[str] = backup_list_file.readlines()
        backup_list_file.close()
    else:
        print('Script running without list of files. Please use "--backup_list" argument.')

    # Create working dir
    try:
        makedirs(WORKING_DIR)
        logging.info('Created %s.', WORKING_DIR)
    except FileExistsError:
        logging.info('%s exist. Skipping.', WORKING_DIR)
    except FileNotFoundError as e:
        logging.info('%s', e)

    create_archive()
    create_hash()
    encrypt_file()
    upload_to_s3(f'{ARCHIVE_NAME}.tar.gpg')
    upload_to_s3(f'{ARCHIVE_NAME}.tar.sha512')
    delete_file(f'{ARCHIVE_NAME}.tar')
    delete_file(f'{ARCHIVE_NAME}.tar.gpg')
    delete_file(f'{ARCHIVE_NAME}.tar.sha512')
